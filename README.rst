OpenCM3 Devcontainer
====================

This Devcontainer contains the libOpenCM3 library and the arm-none-eabi toolchain. The location of libOpenCM3 is set to the environment variable `OPENCM3_DIR` `/opt/libopencm3`