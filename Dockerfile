ARG BASE_IMAGE=uprev/base
ARG BASE_TAG=ubuntu-22.04
FROM ${BASE_IMAGE}:${BASE_TAG}



RUN apt-get update && apt-get install -y --fix-missing --no-install-recommends \ 
    #packages here 
    gcc-arm-none-eabi \
    gdb-multiarch \
    libnewlib-arm-none-eabi \
    libstdc++-arm-none-eabi-newlib \
    openocd \
    python-is-python3 \
    && \
    apt-get autoremove -y && \
    rm -rf /var/lib/apt/lists/* 

ARG LIBOPENCM3_VERSION=v0.8.0

#Clone libOpenCM3
WORKDIR /opt
RUN git clone https://github.com/libopencm3/libopencm3.git --branch ${LIBOPENCM3_VERSION} libopencm3 \
    && cd libopencm3 \
    && make 

COPY  rules.mk /opt/libopencm3/rules.mk
COPY include.mk /opt/libopencm3/include.mk

ENV OPENCM3_DIR=/opt/libopencm3
ENV OPENCM3_INCLUDE_DIR=${OPENCM3_DIR}/include
